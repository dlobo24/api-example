# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* Learn how to make a Rails API
### How do I get set up? ###

#### Rails ####

1. Get SQLite

2. Get RVM (unless you already got it), info in [here](https://rvm.io/rvm/install)

3. Install the correct Ruby version (currently 2.4.1) -> `rvm install ruby-2.4.1`

4. Install the Bundler gem -> `gem install bundler`

5. Install the other gems -> `bundle` OR `bundle install`

6. Setup the database -> `rake db:setup`

### Run the local server ####

14. Run the server -> `rails s`, this will start your local server at `localhost:3000/`


### Contribution guidelines ###

* To handle JSON responses, this project uses the AMS gem, if unfamiliar with it, you can find a guide [here](https://github.com/rails-api/active_model_serializers)

To handle JSON responses, this project uses too the JBuilder gem, if unfamiliar with it, you can find a guide [here](https://github.com/rails/jbuilder)

### Who do I talk to? ###

* Repo owner or admin

* Other community or team contact
