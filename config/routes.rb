# frozen_string_literal: true

Rails.application.routes.draw do
  resources :persons
  resources :countries
end
