# frozen_string_literal: true

class Country < ApplicationRecord
  has_many :persons, dependent: :destroy

  validates :name, uniqueness: true
end
