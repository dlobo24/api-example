
class PersonsController < ApplicationController
  before_action :set_person, only: [:show, :update, :destroy]

  def index
    @persons = Person.all
  end

  def show
    @person = Person.find(params[:id])
  end

  def create
    @person = Person.create(person_params)
    render :show
  end

  def update
    @person.update(person_params)
    render :show
  end

  def destroy
    @person.destroy
    render json: { destroyed: true }
  end

  private

  def person_params
    params.require(:person).permit(:name, :last_name, :age, :country_id)
  end

  def set_person
    @person = Person.find(params[:id])
  end
end
