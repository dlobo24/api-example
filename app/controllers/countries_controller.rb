# frozen_string_literal: true

class CountriesController < ApplicationController
  before_action :set_country, only: %i[show update destroy]

  def index
    @countries = Country.all
    render json: @countries
  end

  def show
    render json: @country
  end

  def create
    @country = Country.create(country_params)
    render json: @country
  end

  def update
    render json: { updated: @country.update(country_params) }
  end

  def destroy
    @country.destroy
    render json: { destroyed: true }
  end

  private

  def country_params
    params.require(:country).permit(:name)
  end

  def set_country
    @country = Country.find(params[:id])
  end
end
