# frozen_string_literal: true

json.persons do
  json.array! @persons.each do |person|
    json.id person.id
    json.name person.name
    json.last_name person.last_name
    json.age person.age
  end
end
