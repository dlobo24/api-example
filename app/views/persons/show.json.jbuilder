# frozen_string_literal: true

json.id @person.id
json.name @person.name
json.last_name @person.last_name
json.age @person.age
